function initOccamVizGraphs(context) {
  'use strict';

  var OccamViz = context.OccamViz;

  /**
   * The graphs submodule constructor.
   * @param {Object} opt_config Contains any properties that should be used to
   * configure this instance of the library.
   * @constructor
   */
  var submodule = OccamViz.Graphs = function(opt_config) {
    opt_config = opt_config || {};
  };

  submodule._graphTypes = [];

  /**
   * Returns the graph types available.
   * @returns {Array} An array of objects describing each graph type available.
   * The entry contains a "name", "description" which has a longer description
   * of the graph type.
   */
  submodule.getGraphs = function() {
    return submodule._graphTypes;
  };

  // LIBRARY PROTOTYPE EXTENSIONS

  // PRIVATE METHODS

  /**
   * Adds the given graph type to the list of graphs available. This list
   * is returned with OccamViz.Graphs.getGraphs().
   * @param {String} graphInfo.name The name of the graphing option.
   * @param {String} graphInfo.description A long description of the graphing
   * option.
   */
  submodule._addGraph = function(graphInfo) {
    OccamViz.Graphs._graphTypes.push(graphInfo);
  };
}
