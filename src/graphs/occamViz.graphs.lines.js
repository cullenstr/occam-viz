/*global $, d3*/
function initOccamVizGraphsLines(context) {

  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "Line Chart",
    call:        "lines",
    description: "A line chart."
  });

  /**
   * Draws a line graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.lines = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    var chart_attr = {
      margin: {
        top:    options.margin.top,
        left:   options.margin.left + 50,
        right:  options.margin.right,
        bottom: options.margin.bottom + (25 * (maxGroups+3) / 3)
      }
    };

    chart_attr.width  = options.width  - chart_attr.margin.left - chart_attr.margin.right;
    chart_attr.height = options.height - chart_attr.margin.top  - chart_attr.margin.bottom;

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
      .attr('width',  options.width  + 'px')
      .attr('height', options.height + 'px');

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate(' + chart_attr.margin.left + ', ' + chart_attr.margin.top + ')');

    var x_series = d3.scale.ordinal()
                     .domain(options.labels.x)
                     .rangeBands([0, chart_attr.width]);

    var y = d3.scale.linear()
              .domain(options.domains.y.reverse())
              .rangeRound([0, chart_attr.height]);

    var x = d3.scale.linear()
                    .domain(options.domains.x)
                    .rangeRound([20, chart_attr.width]);

    var lineFunction = d3.svg.line()
      .x(function(d, i) { return x(i); })
      .y(function(d) { return y(d); })
      .interpolate("linear");

    // Lines
    var lines = chart.append('g')
                     .attr('class', 'lines');

    lines.selectAll('path')
      .data(options.data.groups)      // For every x axis entry
      .enter().append("path")
        .datum(function(d) { return d.series; })
        .attr("class", function(d, i) {
          return "line line-"+i;
        })
        .attr("d", lineFunction)
        .attr('data-group-index', function(d, i) {
          return i;
        })
        .style({
          "fill": "none",
          "stroke": function(d, i) {
            return options.colors[i];
          },
          "stroke-width": "2px"
        });

    // Axis
    var xAxis = d3.svg.axis()
      .scale(x)
      .tickSize(6, 3, 1)
      .orient('bottom');

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + chart_attr.height + ')')
      .call(xAxis)
      .append("text")
        .attr('class', 'x axis-label')
        .style({
          "text-anchor": "middle"
        })
        .attr("transform", 'translate('+ chart_attr.width/2+')')
        .attr("x", 7)
        .attr("dx", ".71em")
        .text(options.labels.x);

    var yAxis = d3.svg.axis()
      .scale(y)
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .attr('class', 'y axis-label')
      .style({
        "text-anchor": "end"
      })
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text(options.labels.y);

    var legend = chart.selectAll(".legend")
      .data(options.data.groups)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        var x = (chart_attr.width / 3) * (i % 3);
        var y = chart_attr.height + 10 + (25*Math.floor((i+3)/3));
        return "translate(" + x + ", " + y + ")";
      });

    legend.append("text")
      .attr("dy", ".35em")
      .attr("transform", "translate(23,0)")
      .style("text-anchor", "start")
      .text(function(d) { return d.name; });

    legend.append("rect")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("transform", "translate(0,-9)")
			.attr('data-group-index', function(d, i) { return i; })
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d, i){
        return options.colors[i];
      });

    $(options.selector + " svg g.legend rect")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke":       "black",
          "stroke-width": "3px"
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": "5px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke":       "",
          "stroke-width": ""
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": ""
          });
      });

    $(options.selector + " svg g.lines g.group path.line")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke-width": "5px"
        });

        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "black",
            "stroke-width": "3px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke-width": ""
        });

        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "",
            "stroke-width": ""
          });
      });
  };
}

