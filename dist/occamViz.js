/*! lib-occamViz - v0.1.0 - 2014-11-20 - OCCAM */
;(function (global) {

/*jslint browser: true*/
/*global $*/

// Compiler directive for UglifyJS.  See occamViz.const.js for more info.
if (typeof DEBUG === 'undefined') {
  DEBUG = true;
}

// LIBRARY-GLOBAL CONSTANTS
//
// These constants are exposed to all library modules.

// GLOBAL is a reference to the global Object.
var Fn = Function, GLOBAL = new Fn('return this')();

// LIBRARY-GLOBAL METHODS
//
// The methods here are exposed to all library modules.  Because all of the
// source files are wrapped within a closure at build time, they are not
// exposed globally in the distributable binaries.

/**
 * A no-op function.  Useful for passing around as a default callback.
 */
function noop () { }

/**
 * Init wrapper for the core module.
 * @param {Object} The Object that the library gets attached to in
 * occamViz.init.js. If the library was not loaded with an AMD loader such as
 * require.js, this is the global Object.
 */
function initOccamVizCore (context) {
  'use strict';

  // PRIVATE MODULE CONSTANTS
  //

  // An example of a CONSTANT variable;
  var CORE_CONSTANT = true;

  // PRIVATE MODULE METHODS
  //
  // These do not get attached to a prototype. They are private utility
  // functions.

  /**
   * This is the constructor for the OccamViz Object.
   * Note that the constructor is also being
   * attached to the context that the library was loaded in.
   * @param {Object} opt_config Contains any properties that should be used to
   * configure this instance of the library.
   * @constructor
   */
  var OccamViz = context.OccamViz = function (opt_config) {
    opt_config = opt_config || {};

    opt_config.domain = opt_config.domain || "";

    // Read only (led by underscores)
    this._domain = opt_config.domain;

    // Accessible
    this.graphs = new OccamViz.Graphs();

    return this;
  };

  // LIBRARY PROTOTYPE METHODS
  //
  // These methods define the public API.

  /**
   * Returns the set domain. An empty string means it is polling the source
   * domain for the script.
   * @return {string}
   */
  OccamViz.prototype.getDomain = function() {
    return this._domain;
  };

  /**
   * Callback for rendering results.
   *
   * @callback resultsCallback
   * @param {Object} results An object defining the results.
   */

  /**
   * Returns the results for the given experiment.
   * @param {number} experiment_id The id of the experiment to query.
   * @param {resultsCallback} success A callback that fires when the results
   * are available.
   * @return {OccamViz}
   */
  OccamViz.prototype.getResults = function(experiment_id, success) {
    var url = this._domain + "/experiments/" + experiment_id + "/results";
    $.getJSON(url, function(data) {
      success(data);
    });

    return this;
  };

  /**
   * This method will sanitize a graph options hash. Typically used internally
   * to fill in the hash with default values, but can be used externally to see
   * default values or use with detached (unregistered) graphing methods.
   */
  OccamViz.sanitizeOptions = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
	options.labels.x = options.labels.x || {};
    options.labels.x.series = options.labels.x.series || [];
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    // Set up domains
    var maxPoint = d3.max(options.data.groups, function( d ) {
      return d3.max(d.series);
    });

    var minPoint = d3.min(options.data.groups, function( d ) {
      return d3.min(d.series);
    });

    var minX = 0;
    var maxX = d3.max([options.labels.x.length - 1,
                       d3.max(options.data.groups, function(d) {
                          return d.series.length - 1;
                       })
                      ]);

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    options.domains.x = options.domains.x || [minX, maxX];
    options.domains.y = options.domains.y || [d3.min([0, minPoint]), maxPoint];

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    options.margin.top    = options.margin.top    || 20;
    options.margin.left   = options.margin.left   || 20;
    options.margin.right  = options.margin.right  || 20;
    options.margin.bottom = options.margin.bottom || 20;

    return options;
  };

  // DEBUG CODE
  //
  // With compiler directives, you can wrap code in a conditional check to
  // ensure that it does not get included in the compiled binaries.  This is
  // useful for exposing certain properties and methods that are needed during
  // development and testing, but should be private in the compiled binaries.
  if (DEBUG) {
  }
}

/*global $, d3*/

function initOccamVizGraphsBarAvg(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "Bar with Averages",
    call:        "barAvg",
    description: "A Bar Chart with averages."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.barAvg = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    // Calculate average
    var avgData = [];
    if (options.data.groups.length > 0) {
      avgData = options.data.groups[0].series.map(function (e, index) {
        var acc = 0;
        options.data.groups.forEach(function (group) {
          acc += group.series[index];
        });
        return acc / options.data.groups.length;
      });
    }

    var series = options.data.groups.map(function (e) {
      var dict = {};
      dict.name = e.name;
      dict.type = 'column';
      dict.data = e.series;
      return dict;
    }).concat({
      name: 'Average',
      type: 'spline',
      data: avgData
    });

    // Set up the chart
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: $(options.selector).attr("id"),
        type: 'column',
        margin: 75,
      },

      plotOptions: {
        column: {
          depth: 25
        }
      },

      colors: options.colors,
      series: series,
    });
  };
}

/*global $, d3*/
function initOccamVizGraphsBars(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "Bar Chart",
    call:        "bars",
    description: "A bar chart.",
    fields: [
      {
        id: "foo",
        label: "Foo",
        default: 15,
        type: "int"
      },
      {
        id: "bar",
        label: "Bar",
        default: "blah",
        type: "string"
      },
      {
        id: "baz",
        label: "Baz",
        default: 10,
        type: "range",
        min: 0,
        max: 10
      },
      {
        id: "grid",
        label: "Enable Grid",
        default: false,
        type: "boolean"
      }
    ]
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.bars = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    // Set up domains
    var minX = 0;
    var maxX = d3.max([options.labels.x.series.length - 1,
                       d3.max(options.data.groups, function(d) {
                          return d.series.length - 1;
                       })
                      ]);

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    var chart_attr = {
      margin: {
        top:    options.margin.top,
        left:   options.margin.left + 50,
        right:  options.margin.right,
        bottom: options.margin.bottom + (25 * (maxGroups+3) / 3) + 20
      }
    };

    chart_attr.width  = options.width  - chart_attr.margin.left - chart_attr.margin.right;
    chart_attr.height = options.height - chart_attr.margin.top  - chart_attr.margin.bottom;

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
      .attr('width',  options.width  + 'px')
      .attr('height', options.height + 'px');

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate(' + chart_attr.margin.left + ', ' + chart_attr.margin.top + ')');

    var popup = svg.append('g')
                   .attr('visibility', 'hidden');

    popup.append('rect')
         .attr('height', '1.2em')
         .attr('x', 0)
         .attr('y', 0)
         .attr('width', '100px')
         .attr('fill', 'red');
    popup.append('text')
         .text('foo')
         .attr('x', 50)
         .attr('y', '1.0em')
         .attr('fill', 'white')
         .style({
           "text-anchor": "middle"
         });

    var x_series = d3.scale.ordinal()
                     .domain(options.labels.x.series)
                     .rangeBands([0, chart_attr.width]);

    var y = d3.scale.linear()
              .domain(options.domains.y.reverse())
              .rangeRound([0, chart_attr.height]);

    // Bars
    var seriesSpace   = chart_attr.width / (maxX+1);
    var seriesPadding = seriesSpace * 0.1;
    var seriesWidth   = seriesSpace - (seriesPadding * 2);

    var barSpace      = seriesWidth / options.data.groups.length;
    var barPadding    = barSpace * 0.1;
    var barWidth      = barSpace - (barPadding * 2);

    options.data.groups.forEach(function(e, i) {
      e.group_index = i;
    });
	
    // Axis
    var xAxis = d3.svg.axis()
      .scale(x_series)
      .tickSize(6, 3, 1)
      .tickValues(options.labels.x.series);

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + chart_attr.height + ')')
      .call(xAxis)
      .append("text")
        .attr('class', 'x axis-label')
        .style({
          "text-anchor": "middle"
        })
        .attr("transform", 'translate('+ chart_attr.width/2+', 38)')
        .text(options.labels.x.name);

    var yAxis = d3.svg.axis()
      .scale(y)
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .attr('class', 'y axis-label')
      .style({
        "text-anchor": "end"
      })
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text(options.labels.y);

    var legend = chart.selectAll(".legend")
      .data(options.data.groups)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        var x = (chart_attr.width/3) * (i % 3) + seriesPadding + barPadding;
        var y = chart_attr.height + 30 + (25*Math.floor((i+3)/3));
        return "translate(" + x + ", " + y + ")";
      });

    legend.append("text")
      .attr("dy", ".35em")
      .attr("transform", "translate(23,0)")
      .style("text-anchor", "start")
      .text(function(d) { return d.name; });

    legend.append("rect")
      .attr("class", "legend")
      .attr("data-group-index", function(d, i) { return i; })
      .attr("transform", "translate(0,-9)")
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d, i){
        return options.colors[i];
      });
	
    var showAllBars;
    var draw_bars = function() {
      // Form a new array selecting only groups that are visible
      var data_array = options.data.groups.filter(function(element) {
        return element.visible == undefined || element.visible;
      });

      seriesSpace   = chart_attr.width / (maxX+1);
      seriesPadding = seriesSpace * 0.1;
      seriesWidth   = seriesSpace - (seriesPadding * 2);

      barSpace      = seriesWidth / data_array.length;
      barPadding    = barSpace * 0.1;
      barWidth      = barSpace - (barPadding * 2);

      var bars = chart.append('g')
                      .attr('class', 'bars');

      bars.selectAll('g')
        .data(d3.range(minX, maxX+1))      // For every x axis entry
        .enter().append('svg:g')     // Create a group
          .attr('class', 'group')
          .attr('transform', function(d, i) {
            return 'translate(' + (seriesSpace * i) + ', 0)';
          })
          .selectAll('rect')
          .data(data_array) // For every group
          .enter().append('rect')    // Create a rectangle
            .attr('x', function(d, i) {
              return seriesPadding + barSpace * i + barPadding;
            })
            .attr('y', function(d, i, j) {
              // i is the index of the group we are in
              // j is the index of our x-axis position
              return y(d.series[j]) + 0.5;
            })
            .attr('data-group-index', function(d, i) { return d.group_index; })
            .attr('data-series-index', function(d, i, j) { return j; })
            .attr('width', barWidth)
            .attr('height', function(d, i, j) {
              return chart_attr.height - y(d.series[j]);
            })
            .style({
              fill: function(d, i) {
                return options.colors[d.group_index];
              }
            });
      // interactive properties of the bars
      $(options.selector + " svg g.bars g.group rect")
        .on('mouseenter', function(e) {
          // Update the text
          var group_index = parseInt($(this).data("group-index"));
          var series_index = parseInt($(this).data("series-index"));
          popup.select('text').text(options.data.groups[group_index].series[series_index]);

          // Measure the width of the text node
          var text_width = popup.select('text').node().getComputedTextLength();

          // Update the width the width of the rectangle
          var width = text_width + 20;
          var text_x = width / 2;
          popup.select('text').attr('x', text_x);
          popup.select('rect').attr('width', width);

          // Move the group into place
	  var graph_position = $(options.selector).position();
          var offsetX = e.pageX - graph_position.left;
          var offsetY = e.pageY - graph_position.top;
          popup.attr('transform', 'translate(' + (offsetX - text_x) + ',' + (offsetY - 40) + ')');
          popup.attr('visibility', 'visible');
          d3.select(options.selector + " rect.legend[data-group-index='" + $(this).data("group-index") + "']")
            .style({
              "stroke":       "black",
              "stroke-width": "3px"
            });
        })

        .on('mouseout', function(e) {
          popup.attr('visibility', 'hidden');
          d3.select(options.selector + " rect.legend[data-group-index='" + $(this).data("group-index") + "']")
            .style({
              "stroke":       "",
              "stroke-width": ""
            });
        })

        .on('mousemove', function(e) {
          var text_node = popup.select('text');
          var text_x = parseInt(text_node.attr('x'));
	  var graph_position = $(options.selector).position();
          var offsetX = e.pageX - graph_position.left;
          var offsetY = e.pageY - graph_position.top;
	  popup.attr('transform', 'translate(' + (offsetX - text_x) + ',' + (offsetY - 40) + ')');
        })
	
	$(options.selector + " svg g.bars g.group rect")
	.on('click', function(e) {
          $(options.selector + ' svg g.bars').remove();
	  var group_index = parseInt($(this).data("group-index"));
	  var bars_visible = 0;

	  options.data.groups.forEach(function(element) { if (element.visible == true) bars_visible++;});
	  if (bars_visible == 1) {
	    options.data.groups.forEach(function(element) { element.visible = true;});
	
	    d3.selectAll(options.selector + " rect.legend")
            .style({
	      "stroke-opacity": "1",
              "fill-opacity":   "1"
            });
	    
	  }
	  else {
	    showAllBars = true;
            options.data.groups.forEach(function(element) { element.visible = false;});
            options.data.groups[group_index].visible = true;

	    d3.selectAll(options.selector + " rect.legend")
              .style({
                "stroke-width":   "",
	        "stroke-opacity": ".25",
                "fill-opacity":   ".25"
              });

            d3.select(options.selector + " rect.legend[data-group-index='" + $(this).data("group-index") + "']")
              .style({
                "stroke-width":   "3px",
	        "stroke-opacity": "1",
                "fill-opacity":   "1"
              });
	  }
	  
	  popup.attr('visibility', 'hidden');
          d3.select(options.selector + " rect.legend[data-group-index='" + $(this).data("group-index") + "']")
            .style({
              "stroke":       "",
              "stroke-width": ""
            });

	  draw_bars();	
	});
    }; //end draw_bars
	
      // interactive properties of the legend
      $(options.selector + " rect.legend")
	.on('mouseenter', function(e) {
          d3.select(this)
            .style({
              "stroke":       "black",
              "stroke-width": "3px"
            });
        })

	.on('mouseout', function(e) {
          d3.select(this)
            .style({
              "stroke":       "",
              "stroke-width": ""
            });
        })

	.on('click', function(e) {
	  var group_index = parseInt($(this).data("group-index"));
	  if (options.data.groups[group_index].visible == false) {
	    $(options.selector + ' svg g.bars').remove();
            options.data.groups[group_index].visible = true;

            d3.select(this)
	      .style({
		"stroke":       "black",
                "stroke-width": "3px",
	        "stroke-opacity": "1",
                "fill-opacity":   "1"
	      });
	  }
	  else if (options.data.groups[group_index].visible == undefined
		   || options.data.groups[group_index].visible == true) {
	    $(options.selector + ' svg g.bars').remove();
            var group_index = parseInt($(this).data("group-index"));
            options.data.groups[group_index].visible = false;
	
            d3.select(options.selector + " rect.legend[data-group-index='" + $(this).data("group-index") + "']")
              .style({
	        "stroke-opacity": ".25",
                "fill-opacity":   ".25"
              });
	  }

	  draw_bars(); 
      });

    draw_bars();
  };
}

/*global $, d3, Highcharts*/

function initOccamVizGraphsBars3d(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "3D Bar Chart",
    call:        "bars3d",
    description: "A 3D bar chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.bars3d = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

    // Set up the chart
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: $(options.selector).attr("id"),
        type: 'column',
        margin: 75,
        options3d: {
          enabled: true,
        alpha: 15,
        beta: 15,
        depth: 50,
        viewDistance: 25
        }
      },
      title: {
        text: 'Chart rotation demo'
      },
      subtitle: {
        text: 'Test options by dragging the sliders below'
      },
      plotOptions: {
        column: {
          depth: 25
        }
      },
      colors: options.colors,
      series: options.data.groups.map(function (e){

        var dict = {};
        dict.name = e.name;
        dict.data = e.series;
        return dict;

      })
    });

    // Activate the sliders
    $('#R0').on('change', function(){
      chart.options.chart.options3d.alpha = this.value;
      showValues();
      chart.redraw(false);
    });
    $('#R1').on('change', function(){
      chart.options.chart.options3d.beta = this.value;
      showValues();
      chart.redraw(false);
    });

    function showValues() {
      $('#R0-value').html(chart.options.chart.options3d.alpha);
      $('#R1-value').html(chart.options.chart.options3d.beta);
    }

    showValues();
  };
}

function initOccamVizGraphs(context) {
  'use strict';

  var OccamViz = context.OccamViz;

  /**
   * The graphs submodule constructor.
   * @param {Object} opt_config Contains any properties that should be used to
   * configure this instance of the library.
   * @constructor
   */
  var submodule = OccamViz.Graphs = function(opt_config) {
    opt_config = opt_config || {};
  };

  submodule._graphTypes = [];

  /**
   * Returns the graph types available.
   * @returns {Array} An array of objects describing each graph type available.
   * The entry contains a "name", "description" which has a longer description
   * of the graph type.
   */
  submodule.getGraphs = function() {
    return submodule._graphTypes;
  };

  // LIBRARY PROTOTYPE EXTENSIONS

  // PRIVATE METHODS

  /**
   * Adds the given graph type to the list of graphs available. This list
   * is returned with OccamViz.Graphs.getGraphs().
   * @param {String} graphInfo.name The name of the graphing option.
   * @param {String} graphInfo.description A long description of the graphing
   * option.
   */
  submodule._addGraph = function(graphInfo) {
    OccamViz.Graphs._graphTypes.push(graphInfo);
  };
}

/*global $, d3*/
function initOccamVizGraphsLines(context) {

  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "Line Chart",
    call:        "lines",
    description: "A line chart."
  });

  /**
   * Draws a line graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.lines = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    var chart_attr = {
      margin: {
        top:    options.margin.top,
        left:   options.margin.left + 50,
        right:  options.margin.right,
        bottom: options.margin.bottom + (25 * (maxGroups+3) / 3)
      }
    };

    chart_attr.width  = options.width  - chart_attr.margin.left - chart_attr.margin.right;
    chart_attr.height = options.height - chart_attr.margin.top  - chart_attr.margin.bottom;

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
      .attr('width',  options.width  + 'px')
      .attr('height', options.height + 'px');

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate(' + chart_attr.margin.left + ', ' + chart_attr.margin.top + ')');

    var x_series = d3.scale.ordinal()
                     .domain(options.labels.x)
                     .rangeBands([0, chart_attr.width]);

    var y = d3.scale.linear()
              .domain(options.domains.y.reverse())
              .rangeRound([0, chart_attr.height]);

    var x = d3.scale.linear()
                    .domain(options.domains.x)
                    .rangeRound([20, chart_attr.width]);

    var lineFunction = d3.svg.line()
      .x(function(d, i) { return x(i); })
      .y(function(d) { return y(d); })
      .interpolate("linear");

    // Lines
    var lines = chart.append('g')
                     .attr('class', 'lines');

    lines.selectAll('path')
      .data(options.data.groups)      // For every x axis entry
      .enter().append("path")
        .datum(function(d) { return d.series; })
        .attr("class", function(d, i) {
          return "line line-"+i;
        })
        .attr("d", lineFunction)
        .attr('data-group-index', function(d, i) {
          return i;
        })
        .style({
          "fill": "none",
          "stroke": function(d, i) {
            return options.colors[i];
          },
          "stroke-width": "2px"
        });

    // Axis
    var xAxis = d3.svg.axis()
      .scale(x)
      .tickSize(6, 3, 1)
      .orient('bottom');

    chart.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0, ' + chart_attr.height + ')')
      .call(xAxis)
      .append("text")
        .attr('class', 'x axis-label')
        .style({
          "text-anchor": "middle"
        })
        .attr("transform", 'translate('+ chart_attr.width/2+')')
        .attr("x", 7)
        .attr("dx", ".71em")
        .text(options.labels.x);

    var yAxis = d3.svg.axis()
      .scale(y)
      .tickSize(6, 3, 1)
      .orient('left');

    chart.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
      .append("text")
      .attr('class', 'y axis-label')
      .style({
        "text-anchor": "end"
      })
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .text(options.labels.y);

    var legend = chart.selectAll(".legend")
      .data(options.data.groups)
      .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) {
        var x = (chart_attr.width / 3) * (i % 3);
        var y = chart_attr.height + 10 + (25*Math.floor((i+3)/3));
        return "translate(" + x + ", " + y + ")";
      });

    legend.append("text")
      .attr("dy", ".35em")
      .attr("transform", "translate(23,0)")
      .style("text-anchor", "start")
      .text(function(d) { return d.name; });

    legend.append("rect")
      .attr("class", function(d, i) {
        return "legend-" + i;
      })
      .attr("transform", "translate(0,-9)")
			.attr('data-group-index', function(d, i) { return i; })
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", function(d, i){
        return options.colors[i];
      });

    $(options.selector + " svg g.legend rect")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke":       "black",
          "stroke-width": "3px"
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": "5px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke":       "",
          "stroke-width": ""
        });

        d3.select(options.selector + " svg g.lines g.group path.line-"+ $(this).data("group-index"))
          .style({
            "stroke-width": ""
          });
      });

    $(options.selector + " svg g.lines g.group path.line")
      .on('mouseenter', function(e) {
        $(this).css({
          "stroke-width": "5px"
        });

        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "black",
            "stroke-width": "3px"
          });
        })
      .on('mouseout', function(e) {
        $(this).css({
          "stroke-width": ""
        });

        d3.select(options.selector + " rect.legend-" + $(this).data("group-index"))
          .style({
            "stroke":       "",
            "stroke-width": ""
          });
      });
  };
}


/*global $, d3, Highcharts*/

function initOccamVizGraphsnvDonut(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Donut Chart",
    call:        "nvDonut",
    description: "An nv Donut chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvDonut = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

   var chart;
   var color_index = 0;
   var xCord = 0;
	var n = 0;
	var color_index = 0;
	var mydata = options.data.groups.map(function (e, i){
		
		var dict = {};
		dict.label = e.name;
		dict.value = e.series;
		dict.color = options.colors[i];		
		return dict;
	
	
	});
	
nv.addGraph(function() {
  var chart = nv.models.pieChart()
      .x(function(d) { return d.label })
      .y(function(d) { return d.value })
      .showLabels(true)     //Display pie labels
      .labelThreshold(.05)  //Configure the minimum slice size for labels to show up
      .labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
      .color(options.colors)
	  .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
      ;

   d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})			
        .datum(mydata)
        .transition().duration(350)
        .call(chart);

  return chart;
});


  };
}

/*global $, d3, Highcharts*/

function initOccamVizGraphsnvLine(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Line Chart",
    call:        "nvLine",
    description: "An nv Line chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvLine = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    var chart;
    var color_index = 0;
    var xCord = 0;
    var mydata = options.data.groups.map(function (e) {
      var dict = {};
      dict.key = e.name;
      dict.values = e.series.map(function (f) {
        var d = {};
        d.x = xCord;
        xCord++;
        if (xCord > 3) {
          xCord = 0;
        }
        d.y = f;
        return d;
      });
      dict.color = options.colors[color_index];
      color_index++;
      return dict;
    });

    nv.addGraph(function() {
      chart = nv.models.lineChart()
      .options({
        margin: {left: 100, bottom: 100},
        x: function(d,i) { return i},
        showXAxis: true,
        showYAxis: true,
        transitionDuration: 250
      });

      // chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
      chart.xAxis
        .axisLabel(options.labels.x)
        .tickFormat(d3.format(',.1f'));

      chart.yAxis
        .axisLabel(options.labels.y)
        .tickFormat(d3.format(',.2f'));

      d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})
        .datum(mydata)
        .call(chart);

      return chart;
    });
  };
}

/*global $, d3, Highcharts*/

function initOccamVizGraphsnvLineAndBar(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Line and Bar Chart",
    call:        "nvLineAndBar",
    description: "An nv Line and Bar chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvLineAndBar = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

   var chart;
   var color_index = 0;
   var xCord = 0;
	var n = 0;
	var color_index = 0;
	var mydata = options.data.groups.map(function (e){
		
		var dict = {};
		dict.key = e.name;
		if(n == 0)
		dict.bar = true;
		n++;
		dict.values = e.series.map(function (f){
			var d = [];
			d.push(f.x);
			d.push(f.y);
			return d;
		
		});
		dict.color = options.colors[color_index];
		color_index++;
		return dict;
	
	
	});
	
 

nv.addGraph(function() {
    var chart = nv.models.linePlusBarChart()
      .margin({top: 30, right: 60, bottom: 50, left: 70})
      .x(function(d,i) { return i })
      .y(function(d) { return d[1] })
      .color(d3.scale.category10().range())
      ;

    chart.xAxis
      .showMaxMin(false)
	  .axisLabel(options.labels.x);
    	
	
   chart.y1Axis
      .tickFormat(d3.format(',f'))
	  .axisLabel(options.labels.y);	
	
    chart.y2Axis
      .tickFormat(d3.format(',f'))
	  .axisLabel(options.labels.y);

    //chart.bars.forceY([0]);

     d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})
      .datum(mydata)
      .transition().duration(500)
      .call(chart)
      ;

    nv.utils.windowResize(chart.update);

    return chart;
});



  };
}

/*global $, d3, Highcharts*/

function initOccamVizGraphsnvLineWithView(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Line Chart with View Finder",
    call:        "nvLineWithView",
    description: "An nv Line chart with a View Finder"
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvLineWithView = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

   var chart;
   var color_index = 0;
   var xCord = 0;
	var n = 0;
	var color_index = 0;
	var mydata = options.data.groups.map(function (e, i){
		
		var dict = {};
		dict.key = e.name;
		if(n == 0)
		dict.bar = true;
		n++;
		dict.values = e.series.map(function (f){
			var d = {};
			d.x = f.x;
			d.y = f.y;			
			return d;
		
		});
		dict.color = options.colors[i];
		color_index++;
		return dict;
	
	
	});
	
 nv.addGraph(function() {
  var chart = nv.models.lineWithFocusChart();

  chart.xAxis
    .tickFormat(d3.format(',f'))
	.axisLabel(options.labels.x);
  chart.yAxis
    .tickFormat(d3.format(',.2f'))
	.axisLabel(options.labels.y);
	

  chart.y2Axis
    .tickFormat(d3.format(',.2f'))
	.axisLabel(options.labels.y);

   d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})
    .datum(mydata)
    .transition().duration(500)
    .call(chart)
    ;
	
	console.log(data());

  nv.utils.windowResize(chart.update);

  return chart;
});



  };
}

/*global $, d3, Highcharts*/

function initOccamVizGraphsnvPie(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Pie Chart",
    call:        "nvPie",
    description: "An nv Pie chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvPie = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

   var chart;
   var color_index = 0;
   var xCord = 0;
	var n = 0;
	var color_index = 0;
	var mydata = options.data.groups.map(function (e, i){
		
		var dict = {};
		dict.label = e.name;
		dict.value = e.series;
		dict.color = options.colors[i];
		return dict;
	
	
	});
	
 

nv.addGraph(function() {
  var chart = nv.models.pieChart()
      .x(function(d) { return d.label })
      .y(function(d) { return d.value })
	  .color(options.colors)
      .showLabels(true);

	
	  
    d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})
        .datum(mydata)
        .transition().duration(350)
        .call(chart);
	
	

  return chart;
});


  };
}

/*global $, d3, Highcharts*/

function initOccamVizGraphsnvScatter(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Scatter Chart",
    call:        "nvScatter",
    description: "An nv Scatter chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvScatter = function(options) {
    options = options || {};

    options.data     = options.data     || {};
    options.width    = options.width    || 800;
    options.height   = options.height   || 400;
    options.colors   = options.colors   || [];
    options.margin   = options.margin   || {};
    options.labels   = options.labels   || {};
    options.domains  = options.domains  || {};

    // Default labels
    options.labels.x = options.labels.x || "foo";
    options.labels.y = options.labels.y || "bar";

    // Sanitize data
    if (options.data instanceof Array) {
      var array = options.data;
      options.data = {};
      options.data.groups = [{"series": array}];
    }
    else {
      options.data.groups = options.data.groups || [];
    }

    var minGroups = 0;
    var maxGroups = options.data.groups.length;

    if (options.colors.length < maxGroups) {
      for (var i = options.colors.length; i < maxGroups; i++) {
        options.colors.push("hsl(" + ((360 / maxGroups) * i) + ", 60%, 60%)");
      }
    }

   var chart;
   var color_index = 0;
   var xCord = 0;

	var color_index = 0;
	var data = options.data.groups.map(function (e){
		
		var dict = {};
		dict.key = e.name;
		dict.values = e.series.map(function (f){
			var d = {};
			d.x = f.x;
			d.y = f.y;
			d.size = 5;
			return d;
		
		});
		dict.color = options.colors[color_index];
		color_index++;
		return dict;
	
	
	});
 

nv.addGraph(function() {
  
  var chart = nv.models.scatterChart()
                .showDistX(true)
                .showDistY(true)
                .color(d3.scale.category10().range());

	chart.xAxis.tickFormat(d3.format('.02f'));
	chart.yAxis.tickFormat(d3.format('.02f'));
	chart.xAxis.axisLabel('x');
	chart.yAxis.axisLabel('y');
	chart.xAxis
    .axisLabel(options.labels.x)
    .tickFormat(d3.format(',.1f'));

  chart.yAxis
    .axisLabel(options.labels.y)
    .tickFormat(d3.format(',.2f'))  ;
 
  d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})	
     .datum(data) 
    .transition().duration(500)
      .call(chart);

  nv.utils.windowResize(chart.update);

  return chart;
});



  };
}

/*global $, d3, Highcharts*/

function initOccamVizGraphsnvStackedBar(context) {
  'use strict';

  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "nv Stacked Bar Chart",
    call:        "nvStackedBar",
    description: "An nv Stacked Bar chart."
  });

  /**
   * Draws a bar graph in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   * @param {number} options.margin.left The left margin in pixels for the graph.
   * @param {number} options.margin.right The right margin in pixels for the graph.
   * @param {number} options.margin.top The top margin in pixels for the graph.
   * @param {number} options.margin.bottom The bottom margin in pixels for the graph.
   * @param {number} options.width The width in pixels of the graph.
   * @param {String} options.labels.x The text for the x axis (an array when there are multiple datapoints per x).
   * @param {String} options.labelx.y The text for the y axis.
   * @param {String} options.colors The color (described as CSS) to use for
   * the bars. This is an array when there are multiple datapoints per x.
   * @param {Array} options.data.groups The array of datapoints.
   */

  Graphs.prototype.nvStackedBar = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 3 * 2;

    var chart;
    var color_index = 0;
    var xCord = 0;

    var color_index = 0;
    var data = options.data.groups.map(function (group) {
      var dict = {};
      dict.key = group.name;
      dict.values = group.series.map(function (f) {
        var d = {};
        d.x = f.x;
        d.y = f.y;
        return d;
      });

      dict.color = options.colors[color_index];
      color_index++;

      return dict;
    });

    nv.addGraph(function() {
      var chart = nv.models.multiBarChart();

      chart.xAxis
           .tickFormat(d3.format(',f'))
           .axisLabel(options.labels.x);

      chart.yAxis
           .tickFormat(d3.format(',.1f'))
           .axisLabel(options.labels.y);

      d3.select(options.selector)
        .append('svg:svg')
        .attr("class", "chart")
        .attr('preserveAspectRatio', 'xMidYMid')
        .attr('viewBox', '0 0 ' + options.width + ' ' + options.height)
        .attr('width',  options.width  + 'px')
        .attr('height', options.height + 'px')
		.style({
			width: options.width + 'px',
			height: options.height + 'px'
		})	
        .datum(data)
        .call(chart);

      nv.utils.windowResize(chart.update);

      return chart;
    });
  };
}

/*global $, d3*/
function initOccamVizGraphsPie(context) {
  'use strict';

  var uuid = 0;
  var Graphs = context.OccamViz.Graphs;

  // Add to our repository of graphing options.
  Graphs._addGraph({
    name:        "Pie Chart",
    call:        "pie",
    description: "A pie chart."
  });

  /**
   * Draws a pie chart in the specified div selector.
   * @param {String} options.selector The css selector pointing to the div to
   * append the graph.
   */
  Graphs.prototype.pie = function(options) {
    // Sanitize Data
    options = context.OccamViz.sanitizeOptions(options);
    options.height = options.width / 2;

    var chart_attr = {
      margin: options.margin
    };

    chart_attr.width  = options.width  - chart_attr.margin.left - chart_attr.margin.right;
    chart_attr.height = options.height - chart_attr.margin.top  - chart_attr.margin.bottom;

    // Pie charts
    var radius = 100;
    var pie_layout = d3.layout.pie()
                              .sort(null);

    var svg = d3.select(options.selector)
      .append("svg:svg")
      .attr("class", "pie chart")
      .attr('preserveAspectRatio', 'xMidYMid')
      .attr('viewBox', '0 0 ' + options.width + ' ' + options.height);

    var defs = svg.append('defs');

    var chart_size = d3.min([chart_attr.width, chart_attr.height]);

    var arc = d3.svg.arc()
                    .innerRadius(function(d, i, j) { return j * (chart_size / 2 / options.labels.x.length); })
                    .outerRadius(function(d, i, j) { return (j+1) * (chart_size / 2 / options.labels.x.length); });

    var chart = svg.append("svg:g")
                   .attr('transform', 'translate(' + chart_attr.margin.left + ", "
                                                   + chart_attr.margin.top + ")");

    var pies = chart.append('g')
                    .attr('class', 'pies')
                    .attr('transform', 'translate(' + chart_attr.width / 2 + ', ' + chart_attr.height / 2 + ')');

    pies.selectAll('g.pie')
        .data(options.labels.x)
        .enter().append('svg:g')
          .attr('class', function(d,i) { return 'group pie pie-' + i; })
          .selectAll('path.slice')
          .data(function(d, i) {
            return pie_layout(
              options.data.groups.map(function(e) {
                return e.series[i];
              })
            );
          })
          .enter().append('path')
            .attr('class', function(d, i) { return 'slice slice-' + i; })
            .attr('d', arc)
            .attr('data-group-index', function(d, i) { return i; })
            .style('fill', function(d, i) { return options.colors[i]});

    $(options.selector + " svg g.pies g.pie path.slice")
      .on('mouseenter', function(e) {
        chart.selectAll("g.pies g.pie path.slice")
          .style({
            "opacity":      "0.6",
          });

        chart.selectAll("g.pies g.pie path.slice.slice-" + $(this).data("group-index"))
          .style({
            "opacity":      "1.0",
          });
      })
      .on('mouseout', function(e) {
        chart.selectAll("g.pies g.pie path.slice")
          .style({
            "opacity":      "1.0",
          });
      });

    var labels = chart.append('svg:g')
                      .attr('class', 'series labels');

    // Labels for stacked pie chart
    if (options.labels.x.length > 1) {
      labels.selectAll('line.label')
        .data(options.labels.x)
        .enter().append("svg:line")
          .attr('x1', chart_attr.width / 2)
          .attr('y1', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length);
          })
          .attr('x2', chart_attr.width)
          .attr('y2', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length);
          })
          .attr('class', 'label')
          .attr('stroke', 'rgba(255, 255, 255, 0.7)');

      labels.selectAll('rect.label')
        .data(options.labels.x)
        .enter().append("svg:rect")
          .attr('fill', 'rgba(255, 255, 255, 0.7)')
          .attr('class', 'label')
          .attr('data-series-index', function(d,i) { return options.labels.x.length-1-i; })
          .attr('x', chart_attr.width - ((chart_attr.width - chart_size - 20) / 2))
          .attr('y', function(d, i) {
            return (i+0.5) * (chart_size / 2 / options.labels.x.length) - 30;
          })
          .attr('height', 30)
          .attr('width', (chart_attr.width - chart_size - 20) / 2);

      var label_width = ((chart_attr.width - chart_size - 20) / 2);

      labels.selectAll('text.label')
        .data(options.labels.x)
        .enter().append("svg:text")
          .attr('x', chart_attr.width - label_width/2)
          .attr('y', function(d, i) {
            return (options.labels.x.length-1-i+0.5) * (chart_size / 2 / options.labels.x.length) - 15;
          })
          .attr('text-anchor', 'middle')
          .attr('data-series-index', function(d,i) { return i; })
          .attr('class', 'label')
          .style('alignment-baseline', 'central')
          .style('dominant-baseline', 'central')
          .text(function(d,i) { return d; });

      $(options.selector + " svg g.series.labels .label")
        .on('mouseenter', function(e) {
          chart.selectAll("g.pies g.pie")
            .style({
              "opacity":      "0.6",
            });

          chart.selectAll("g.pies g.pie.pie-" + $(this).data("series-index"))
            .style({
              "opacity":      "",
            });
        })
        .on('mouseout', function(e) {
          chart.selectAll("g.pies g.pie")
            .style({
              "opacity":      "",
            });
        });
    }

    var group_labels = chart.append('svg:g')
                            .attr('class', 'group labels')
                            .attr('transform', 'translate(' + chart_attr.width / 2 + ', ' + chart_attr.height / 2 + ')');

    var groups = options.data.groups.map(function(group) {
      return group.name;
    });

    // Add group labels
    // Put them by default at locations radiating out from graph
    //   from the center of the arcs that correspond
    // Fit them inside the chart, do not allow them to overlap circle
    //   or pie chart legend or group legend (Initially empty)
    // If lines from center of arc to text go through circle or pie
    //   chart legend, put those labels inside group legend
    // add group legend, repeat process until fit
    // Add lines

    var group_arc = d3.svg.arc()
                          .innerRadius(chart_size / 2 + 100)
                          .outerRadius(chart_size / 2 + 100);

    var edge_arc = d3.svg.arc()
                         .innerRadius(chart_size / 2)
                         .outerRadius(chart_size / 2);

    // Assign default label locations around circle
    group_labels.selectAll('text.slice')
      .data(pie_layout(
        options.data.groups.map(function(e) {
          return e.series[e.series.length-1];
        })
      ))
      .enter().append('text')
        .attr('class', 'slice')
        .attr('x', function(d) {
          return group_arc.centroid(d)[0];
        })
        .attr('y', function(d) {
          return group_arc.centroid(d)[1];
        })
        .attr('dy', -5)
        .attr('dx', function(d) {
          if ((d.endAngle + d.startAngle)/2 > Math.PI) {
            return -5;
          }
          else {
            return 5;
          }
        })
        .attr('text-anchor', function(d) {
          if ((d.endAngle + d.startAngle)/2 > Math.PI) {
            return "end";
          }
          else {
            return "start";
          }
        })
        .text(function(d, i) {
          return options.data.groups[i].name;
        })
        .style('fill', "#ddd");

    // Move labels that are not in the graph
    group_labels.selectAll('text').each(function() {
      var old_y = d3.select(this).attr('y');
      if (old_y < -(chart_attr.height/2)+30) {
        old_y = -(chart_attr.height/2)+30;
      }
      if (old_y > (chart_attr.height/2)-30) {
        old_y = (chart_attr.height/2)-30;
      }
      d3.select(this).attr('y', old_y);
    });

    // Move labels that are inside pie legend
    if (options.labels.x.length > 1) {
      var pie_legend = {
        x1: 0,
        y1: -0.5 * (chart_size / 2 / options.labels.x.length),
        x2: chart_attr.width / 2,
        y2: -(options.labels.x.length-0.5) * (chart_size / 2 / options.labels.x.length) - 30
      };

      group_labels.selectAll('text').each(function() {
        var old_y = d3.select(this).attr('y');
        var old_x = d3.select(this).attr('x');
        if (old_x > pie_legend.x1 &&
            old_y > pie_legend.y2 && old_y < pie_legend.y1) {
          old_y = pie_legend.y1 + 30;
        }
        d3.select(this).attr('y', old_y);
      });
    }

    // Add lines
    group_labels.selectAll('line')
      .data(pie_layout(
        options.data.groups.map(function(e) {
          return e.series[e.series.length-1];
        })
      ))
      .enter().append('line')
        .attr('class', 'slice')
        .attr('x1', function(d) {
          return edge_arc.centroid(d)[0];
        })
        .attr('y1', function(d) {
          return edge_arc.centroid(d)[1];
        })
        .attr('x2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('x');
        })
        .attr('y2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .style('stroke', function(d, i) {
          return options.colors[i];
        });

    group_labels.selectAll('line.underline')
      .data(options.data.groups)
      .enter().append('line')
        .attr('class', 'slice underline')
        .attr('x1', function(d,i) {
          var label = group_labels.select('text:nth-child('+(i+1)+')');
          if (label.attr('text-anchor') === 'end') {
            return label.node().getBBox().x;
          }
          else {
            return label.node().getBBox().x + label.node().getBBox().width;
          }
        })
        .attr('y1', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .attr('x2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('x');
        })
        .attr('y2', function(d,i) {
          return group_labels.select('text:nth-child('+(i+1)+')').attr('y');
        })
        .style('stroke', function(d, i) {
          return options.colors[i];
        });

    uuid += 1;
  };
}

/*globals initOccamVizCore*/
/*globals initOccamVizGraphs*/
/*globals initOccamVizGraphsBars*/
/*globals initOccamVizGraphsPie*/
/*globals initOccamVizGraphsLines*/
/*globals initOccamVizGraphsBars3d*/
/*globals initOccamVizGraphsBarAvg*/
/*globals initOccamVizGraphsnvLine*/
/*globals initOccamVizGraphsnvScatter*/
/*globals initOccamVizGraphsnvStackedBar*/
/*globals initOccamVizGraphsnvLineAndBar*/
/*globals initOccamVizGraphsnvPie*/
/*globals initOccamVizGraphsnvDonut*/
/*globals initOccamVizGraphsnvLineWithView*/
var initOccamViz = function (context) {
  initOccamVizCore(context);

  // Add a similar line as above for each module that you have.  If you have a
  // module named "Awesome Module," it should live in the file
  // "src/occamViz.awesome-module.js" with a wrapper function named
  // "initAwesomeModule".
  initOccamVizGraphs(context);
  initOccamVizGraphsBars(context);
  initOccamVizGraphsPie(context);
  initOccamVizGraphsLines(context);
  initOccamVizGraphsBars3d(context);
  initOccamVizGraphsBarAvg(context);
  initOccamVizGraphsnvLine(context);
  initOccamVizGraphsnvScatter(context);
  initOccamVizGraphsnvStackedBar(context);
  initOccamVizGraphsnvLineAndBar(context);
  initOccamVizGraphsnvPie(context);
  initOccamVizGraphsnvDonut(context);
  initOccamVizGraphsnvLineWithView(context);
  return context.OccamViz;
};

if (typeof define === 'function' && define.amd) {
  // Expose OccamViz as an AMD module if it's loaded with RequireJS or
  // similar.
  define(function () {
    return initOccamViz({});
  });
} else {
  // Load OccamViz normally (creating a OccamViz global) if not using an AMD
  // loader.
  initOccamViz(this);
}

// Your library may have many modules.  How you organize the modules is up to
// you, but generally speaking it's best if each module addresses a specific
// concern.  No module should need to know about the implementation details of
// any other module.

// Note:  You must name this function something unique.  If you end up
// copy/pasting this file, the last function defined will clobber the previous
// one.
function initOccamVizModule (context) {
  'use strict';

  var OccamViz = context.OccamViz;

  // A library module can do two things to the OccamViz Object:  It can extend
  // the prototype to add more methods, and it can add static properties.  This
  // is useful if your library needs helper methods.

  // PRIVATE MODULE CONSTANTS
  var MODULE_CONSTANT = true;

  // PRIVATE MODULE METHODS
  //

  /**
   *  An example of a private method.  Feel free to remove this.
   */
  function modulePrivateMethod () {
    return;
  }

  // LIBRARY STATIC PROPERTIES
  //

  /**
   * An example of a static OccamViz property.  This particular static property
   * is also an instantiable Object.
   * @constructor
   */
  OccamViz.OccamVizHelper = function () {
    return this;
  };

  // LIBRARY PROTOTYPE EXTENSIONS
  //
  // A module can extend the prototype of the OccamViz Object.

  /**
   * An example of a prototype method.
   * @return {string}
   */
  OccamViz.prototype.alternateGetReadOnlyVar = function () {
    // Note that a module can access all of the OccamViz instance variables with
    // the `this` keyword.
    return this._readOnlyVar;
  };

  if (DEBUG) {
    // DEBUG CODE
    //
    // Each module can have its own debugging section.  They all get compiled
    // out of the binary.
  }
}

} (this));
