describe("occamViz.graphs", function() {
  var graphTypes = [];

  beforeEach(function() {
    graphTypes = OccamViz.Graphs._graphTypes;
    OccamViz.Graphs._graphTypes = [];
  });

  afterEach(function() {
    OccamViz.Graphs._graphTypes = graphTypes;
  });

  describe("constructor", function() {
    it("should default the graph list to be an empty array", function() {
      expect(OccamViz.Graphs.getGraphs()).toEqual([]);
    });
  });

  describe("getGraphs", function() {
    it("should return items added by _addGraph", function() {
      var graph = {
        name: "foo",
        description: "bar"
      };
      OccamViz.Graphs._addGraph(graph);
      expect(OccamViz.Graphs.getGraphs()[0]).toEqual(graph);
    });
  });
});
